const express = require('express')
const app = express()

/**
 * Add three numbers together and calculate average
 * @param {Number} a first param
 * @param {Number} b second param
 * @param {Number} c third param
 * @returns {Number} average of a, b and c
 */
const add = (a, b, c) => {
  return (a + b + c) / 3
}

app.get('', (req, res) => {
  const average = add(8, 2, 6)
  console.log(average)
  res.send('ok')
})

app.listen(3000, () => {
  console.log('Server listening on localhost:3000')
})